import React, { Component } from 'react';
import data from "./datas.json"
import { ListGloutons, Settings } from './components';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

export class FournilPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      searchInput: "",
      listGloutons: this.returnGloutons(),
      isOpenModal: false,
      glouton: {},
      passif: false,
      pains: data.pains
    }
  }


  render() {
    const { searchInput, isOpenModal, glouton, value, pains } = this.state
    const filter = {
      searchInput: searchInput,
      isOpenModal: isOpenModal,
      glouton: glouton,
      handleChange: this.handleChange,
      handleOpen: this.handleOpen,
      handleClose: this.handleClose,
      gloutonFilter: this.gloutonFilter,
      addModifGlouton: this.addModifGlouton,
      edit: this.edit,
      erase: this.erase,
      setPassif: this.setPassif,
      pains: pains,
    }
    return(
      <div>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChangeTab}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            scrollButtons="auto"
          >
            <Tab label="Gloutons" />
            <Tab label="En attentes" />
            <Tab label="Paramètres" />
          </Tabs>
        </AppBar>
        {value === 0 && <ListGloutons filter={filter} passif={false} />}
        {value === 1 && <ListGloutons filter={filter} passif={true} />}
        {value === 2 && <Settings pains={pains} />}
      </div>
    )
  }

  returnGloutons = () => {
    let newList = []
    for (let i = 0; i < data.gloutons.length; i++) {
      newList.push({...data.gloutons[i], id: i})
    }
    return newList
  }

  handleChangeTab = (event, value) => {
    this.setState({ value });
  };

  handleChange = (e) => {
    let userInput = e.target.value
    this.setState({searchInput: userInput})
  }

  handleOpen = () => {
    this.setState({ isOpenModal: true, glouton: {} })
  };

  handleClose = () => {
    this.setState({ isOpenModal: false })
  };

  gloutonFilter = (passif) => {
    let userInput = this.state.searchInput
    let list = this.state.listGloutons
    let ele = list.filter(
      el => {
        let name = ""
        if (el.nom !== undefined) {
          name = el.nom.toLowerCase()
        }
        if (el.passif === passif) {
          return name.indexOf(userInput.toLowerCase()) !== -1
        } else {
         return null
        }
      })
      return ele.sort((a,b) => { return a.nom > b.nom })
    }

  addModifGlouton = (glouton) => {
    let { listGloutons } = this.state
    if (glouton.id === undefined) {
      this.setState({listGloutons: [...listGloutons, {...glouton, id: listGloutons.length}], isOpenModal: false})
    } else {
      listGloutons.splice(this.getIndexFromId(glouton.id), 1)
      listGloutons.push(glouton)
      this.setState({listGloutons, isOpenModal: false})
    }
  }

  getIndexFromId = (id) => {
    let { listGloutons } = this.state
    for (let i = 0; i < listGloutons.length; i++) {
      if (id === listGloutons[i].id) {
        return i
      }
    }
  }

  edit = (glouton) => {
    this.setState({ isOpenModal: true, glouton: glouton})
  }

  erase = (glouton) => {
    const listGloutonsCopy = this.state.listGloutons
    listGloutonsCopy.splice(this.getIndexFromId(glouton.id), 1)
    this.setState({listGloutons: listGloutonsCopy})
  }

  setPassif = (glouton) => {
    const { listGloutons } = this.state
    listGloutons[this.getIndexFromId(glouton.id)].passif = !listGloutons[this.getIndexFromId(glouton.id)].passif
    this.setState({listGloutons: listGloutons})
  }

}
