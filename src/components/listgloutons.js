import React, { Component } from 'react'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

// import Edit from '@material-ui/icons/Edit'
import WatchLater from '@material-ui/icons/WatchLater'
import PersonAdd from '@material-ui/icons/PersonAdd'
import Delete from '@material-ui/icons/Delete'

import { FormModal } from './'


export class ListGloutons extends Component {

  render() {
    const { searchInput, isOpenModal, glouton, handleChange, handleOpen, handleClose, gloutonFilter, addModifGlouton, edit, erase, setPassif, pains } = this.props.filter
    const { passif } = this.props
    const commandes = (commandes) => {
      let list = []
      for (let j in commandes) {
        list.push(<p key={j+"f"} >{j}</p>)
        let list2 = []
        for (let p in commandes[j]) {
          list2.push(<p key={p} >{pains[p].nom}: {commandes[j][p]}</p>)
        }
        list.push(<div key={j} >{list2}</div>)
      }
      return <div>{list}</div>
    }
    return (
      <Paper>
        <TextField
          placeholder="Recherche"
          onChange={handleChange}
          value={searchInput}
        />
      <FormModal addModifGlouton={addModifGlouton} close={handleClose} isOpenModal={isOpenModal} glouton={glouton}/>
        <Table>
          <TableHead>
            <TableRow style={styles.cell}>
              <TableCell style={styles.padding}>info</TableCell>
              <TableCell style={styles.padding}>commandes</TableCell>
              <TableCell>solde</TableCell>
              <TableCell>
                <Button onClick={handleOpen}><PersonAdd /></Button>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {gloutonFilter(passif).map((el, i) =>
              <TableRow key={`${i}` + el.nom}>
                <TableCell><Button onClick={edit.bind(null, el)}>{el.nom}</Button></TableCell>
                <TableCell>{commandes(el.commande)}</TableCell>
                <TableCell>{el.solde} €</TableCell>
                <TableCell>
                  <Button onClick={setPassif.bind(null, el)}><WatchLater /></Button>
                  <Button onClick={erase.bind(null, el)}><Delete /></Button>
                </TableCell>
              </TableRow>
              )
            }
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

const styles = {
  cell: {
    textTransform: "uppercase",
  },
  padding: {
    paddingLeft: "40px",
  }
}
