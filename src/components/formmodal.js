import React from 'react'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'

export class FormModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tel: "",
      mail: "",
      nom: "",
      solde: "",
    };
  }

  collectGlouton = () => {
    const { tel, mail, nom, solde } = this.state
    const { glouton, addModifGlouton } = this.props
    addModifGlouton({nom: nom || glouton.nom, mail: mail || glouton.mail, tel: tel || glouton.tel, solde: solde || glouton.solde, passif: false, id: glouton.id})
    this.setState({tel: "", mail: "", nom: "", solde: ""})
  }

  handleChange = (e) => {
    let input = e.target.value
    let textfieldId = e.target.id
    this.setState({[textfieldId]: input})
  }

  render() {
    const { glouton, close, isOpenModal } = this.props
    return (
      <div>
        <Dialog
          open={isOpenModal}
          onClose={close}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Ajout / Modification d'un Glouton</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="nom"
              label="Nom Prénom"
              defaultValue={glouton.nom}
              fullWidth
              onChange={this.handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              id="mail"
              label="Address email"
              defaultValue={glouton.mail}
              type="email"
              fullWidth
              onChange={this.handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              id="tel"
              label="téléphone"
              defaultValue={glouton.tel}
              fullWidth
              onChange={this.handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              id="solde"
              label="Solde"
              type="number"
              defaultValue={glouton.solde}
              fullWidth
              onChange={this.handleChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={close} color="primary">
              Annuler
            </Button>
            <Button onClick={this.collectGlouton} color="primary">
              Valider
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
