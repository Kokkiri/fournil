import React, { Component } from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';



export class Settings extends Component {

  render() {
    const { pains } = this.props
    return(
      <Paper>
        <Table>
          <TableHead>
            <TableRow style={styles.cell}>
              <TableCell>code</TableCell>
              <TableCell>nom</TableCell>
              <TableCell>poids</TableCell>
              <TableCell>prix unitaires</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              Object.keys(pains).map(
                (el) =>
                <TableRow key={el}>
                  <TableCell>{el}</TableCell>
                  <TableCell>{pains[el].nom}</TableCell>
                  <TableCell>{pains[el].poids}</TableCell>
                  <TableCell>{pains[el].prix} €</TableCell>
                </TableRow>
              )
            }
          </TableBody>

        </Table>
      </Paper>
    )
  }
}

const styles = {
  cell: {
    textTransform: "uppercase",
  }
}
